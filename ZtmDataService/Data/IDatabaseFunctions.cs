namespace ZtmDataService.Data
{
    public interface IDatabaseFunctions
    {
        void ClearDatabase();
        void ConnectFuzzySearchModule();
    }
}
