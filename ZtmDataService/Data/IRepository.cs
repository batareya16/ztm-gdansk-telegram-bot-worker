using System.Collections.Generic;

namespace ZtmDataService.Data
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        void InsertRange(IEnumerable<T> items);
    }
}
