using Microsoft.EntityFrameworkCore;

namespace ZtmDataService.Data
{
    public class DatabaseFunctions : IDatabaseFunctions
    {
        private readonly DatabaseContext databaseContext;

        public DatabaseFunctions(DatabaseContext databaseContext)
        {
            this.databaseContext = databaseContext;
        }

        public void ClearDatabase()
        {
            databaseContext.Database.ExecuteSqlRaw($"TRUNCATE TABLE {nameof(DatabaseContext.Stops)}");
            databaseContext.Database.ExecuteSqlRaw($"TRUNCATE TABLE {nameof(DatabaseContext.Routes)}");
            databaseContext.Database.ExecuteSqlRaw($"TRUNCATE TABLE {nameof(DatabaseContext.TripStops)}");
            databaseContext.Database.ExecuteSqlRaw($"TRUNCATE TABLE {nameof(DatabaseContext.Trips)}");
            databaseContext.Database.ExecuteSqlRaw($"TRUNCATE TABLE {nameof(DatabaseContext.StopsTime)}");
        }

        public void ConnectFuzzySearchModule()
        {
            databaseContext.Database.ExecuteSqlRaw("create extension if not exists pg_trgm");
        }
    }
}
