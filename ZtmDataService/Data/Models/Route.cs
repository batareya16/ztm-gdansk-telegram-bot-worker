using System;
using Newtonsoft.Json;

namespace ZtmDataService.Data.Models
{
    [Serializable]
    public class Route : BaseEntity
    {
        public override DateTime ValidDate { get; set; }

        [JsonProperty("routeId")]
        public long Id { get; set; }

        [JsonProperty("routeShortName")]
        public string ShortName { get; set; }

        [JsonProperty("routeLongName")]
        public string LongName { get; set; }

        [JsonProperty("routeType")]
        public string Type { get; set; }
    }
}
