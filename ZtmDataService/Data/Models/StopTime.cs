using System;
using Newtonsoft.Json;

namespace ZtmDataService.Data.Models
{
    public class StopTime : BaseEntity
    {
        [JsonProperty("date")]
        public override DateTime ValidDate { get; set; }

        [JsonProperty("routeId")]
        public long RouteId { get; set; }

        [JsonProperty("tripId")]
        public long TripId { get; set; }

        [JsonProperty("stopId")]
        public long StopId { get; set; }

        [JsonProperty("arrivalTime")]
        public DateTime ArrivalTime { get; set; }

        [JsonProperty("departureTime")]
        public DateTime DepartureTime { get; set; }

        [JsonProperty("passenger")]
        public bool? IsPassenger { get; set; }
    }
}
