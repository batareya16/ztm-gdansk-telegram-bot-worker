using System;

namespace ZtmDataService.Data.Models
{
    public class BaseEntity
    {
        public virtual DateTime ValidDate { get; set; }
    }
}
