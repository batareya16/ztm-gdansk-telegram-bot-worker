using System;
using Newtonsoft.Json;

namespace ZtmDataService.Data.Models
{
    public class Trip : BaseEntity
    {
        public override DateTime ValidDate { get; set; }

        [JsonProperty("id")]
        public string Code { get; set; }

        [JsonProperty("routeId")]
        public long RouteId { get; set; }

        [JsonProperty("tripId")]
        public long TripId { get; set; }

        [JsonProperty("tripHeadsign")]
        public string Headsign { get; set; }

        [JsonProperty("tripShortName")]
        public string ShortName { get; set; }

        [JsonProperty("directionId")]
        public long Direction { get; set; }

        [JsonProperty("type")]
        public string RouteType { get; set; }
    }
}
