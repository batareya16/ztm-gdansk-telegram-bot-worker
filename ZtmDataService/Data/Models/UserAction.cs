namespace ZtmDataService.Data.Models
{
    public class UserAction
    {
        public long UserId { get; set; }

        public long Timestamp { get; set; }

        public long? ChatId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string LangCode { get; set; }

        public string Message { get; set; }

        public string Callback { get; set; }
    }
}
