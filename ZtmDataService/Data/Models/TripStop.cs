using System;
using Newtonsoft.Json;

namespace ZtmDataService.Data.Models
{
    public class TripStop : BaseEntity
    {
        public override DateTime ValidDate { get; set; }

        [JsonProperty("routeId")]
        public long RouteId { get; set; }

        [JsonProperty("tripId")]
        public long TripId { get; set; }

        [JsonProperty("stopId")]
        public long StopId { get; set; }

        [JsonProperty("stopSequence")]
        public short Order { get; set; }

        [JsonProperty("passenger")]
        public bool? IsPassenger { get; set; }

        [JsonProperty("topologyVersionId")]
        public long TopologyVersionId { get; set; }
    }
}
