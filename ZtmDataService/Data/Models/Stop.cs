using System;
using Newtonsoft.Json;

namespace ZtmDataService.Data.Models
{
    [Serializable]
    public class Stop : BaseEntity
    {
        public override DateTime ValidDate { get; set; }

        [JsonProperty("stopId")]
        public long Id { get; set; }

        [JsonProperty("stopCode")]
        public string Code { get; set; }

        [JsonProperty("stopName")]
        public string Name { get; set; }

        [JsonProperty("stopShortName")]
        public string ShortName { get; set; }

        [JsonProperty("stopDesc")]
        public string Description { get; set; }

        [JsonProperty("subName")]
        public string SubName { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("zoneId")]
        public long? ZoneId { get; set; }

        [JsonProperty("zoneName")]
        public string ZoneName { get; set; }

        [JsonProperty("virtual")]
        public bool? IsVirtual { get; set; }

        [JsonProperty("nonpassenger")]
        public bool? Nonpassenger { get; set; }

        [JsonProperty("depot")]
        public bool? IsDepot { get; set; }

        [JsonProperty("ticketZoneBorder")]
        public bool? IsTicketZoneBorder { get; set; }

        [JsonProperty("onDemand")]
        public bool? IsOnDemand { get; set; }

        [JsonProperty("stopLat")]
        public double Latitude { get; set; }

        [JsonProperty("stopLon")]
        public double Longtitude { get; set; }

        [JsonProperty("stopUrl")]
        public string Url { get; set; }
    }
}
