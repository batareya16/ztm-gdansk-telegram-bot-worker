using System;
using Microsoft.EntityFrameworkCore.Storage;

namespace ZtmDataService.Data
{
    public class TransactionContext : IDisposable
    {
        private readonly IDbContextTransaction transaction;
        private bool isDisposed = false;

        public TransactionContext(DatabaseContext databaseContext)
        {
            transaction = databaseContext.Database.BeginTransaction();
        }

        public void Dispose()
        {
            if (isDisposed)
            {
                return;
            }

            transaction.Commit();
            isDisposed = true;
        }
    }
}
