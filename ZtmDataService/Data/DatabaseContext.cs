using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ZtmDataService.Data.Models;
using ZtmDataService.Options;

namespace ZtmDataService.Data
{
    public class DatabaseContext : DbContext
    {
        private readonly ConnectionOptions connectionOpts;

        public DatabaseContext(DbContextOptions<DatabaseContext> options, IOptions<ConnectionOptions> connectionOpts)
            : base(options)
        {
            this.connectionOpts = connectionOpts.Value;
        }

        public DbSet<Stop> Stops { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<TripStop> TripStops { get; set; }
        public DbSet<StopTime> StopsTime { get; set; }
        public DbSet<UserAction> UserActions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql(connectionOpts.ConnectionString);

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            modelBuilder
                .Entity<Stop>()
                .ToTable("stops")
                .HasKey(x => new { x.ValidDate, x.Id });

            modelBuilder
                .Entity<Route>()
                .ToTable("routes")
                .HasKey(x => new { x.ValidDate, x.Id });

            modelBuilder
                .Entity<Trip>()
                .ToTable("trips")
                .HasKey(x => new { x.ValidDate, x.Code });

            modelBuilder
                .Entity<TripStop>()
                .ToTable("tripstops")
                .HasKey(x => new { x.ValidDate, x.RouteId, x.TripId, x.StopId, x.Order });

            modelBuilder
                .Entity<StopTime>()
                .ToTable("stopstime")
                .HasKey(x => new { x.ValidDate, x.RouteId, x.TripId, x.StopId, x.ArrivalTime });

            modelBuilder
                .Entity<UserAction>()
                .ToTable("useractions")
                .HasKey(x => new { x.Timestamp, x.UserId });

            base.OnModelCreating(modelBuilder);
        }
    }
}
