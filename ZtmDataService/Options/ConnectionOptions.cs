namespace ZtmDataService.Options
{
    public class ConnectionOptions
    {
        public string ConnectionString { get; set; }

        public string ApiBaseAddress { get; set; }
    }
}
