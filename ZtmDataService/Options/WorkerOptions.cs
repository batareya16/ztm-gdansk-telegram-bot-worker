namespace ZtmDataService.Options
{
    public class WorkerOptions
    {
        public long DueHours { get; set; }

        public long Period { get; set; }
    }
}
