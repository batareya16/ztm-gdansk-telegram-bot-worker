using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using ZtmDataService.Data.Models;
using ZtmDataService.Http;

namespace ZtmDataService.Commands.RefreshDataCommand.Rest
{
    public abstract class CommonRestClient<T> where T : BaseEntity
    {
        protected RestClient<T> RestClient { get; }

        protected CommonRestClient(RestClient<T> restClient)
        {
            RestClient = restClient;
        }

        protected async Task<IList<T>> GetAll(string collectionPropertyName)
        {
            var dateRoutes = JObject.Parse(await RestClient.GetAsStringAsync());
            var resultRoutes = new List<T>();

            foreach (var dateRoute in dateRoutes.Properties())
            {
                var date = DateTime.Parse(dateRoute.Name);
                if (date > DateTime.Today.AddDays(1))
                {
                    break;
                }

                resultRoutes.AddRange(dateRoute.Value[collectionPropertyName].ToObject<T[]>().Select(x =>
                {
                    x.ValidDate = date;
                    return x;
                }));
            }

            return resultRoutes;
        }
    }
}
