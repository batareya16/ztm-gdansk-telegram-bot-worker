using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using ZtmDataService.Data.Models;
using ZtmDataService.Http;
using ZtmDataService.Options;

namespace ZtmDataService.Commands.RefreshDataCommand.Rest
{
    public class StopsRestClient : CommonRestClient<Stop>
    {
        public StopsRestClient(IOptions<ConnectionOptions> connectionOpts)
            : base(new RestClient<Stop>(
                connectionOpts.Value.ApiBaseAddress,
                "dataset/c24aa637-3619-4dc2-a171-a23eec8f2172/resource/4c4025f0-01bf-41f7-a39f-d156d201b82b/download/stops.json"))
        {
        }

        public async Task<IList<Stop>> GetAll()
        {
            return await GetAll("stops");
        }
    }
}
