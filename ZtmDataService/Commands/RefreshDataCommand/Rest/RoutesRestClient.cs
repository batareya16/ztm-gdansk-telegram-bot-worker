using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using ZtmDataService.Data.Models;
using ZtmDataService.Http;
using ZtmDataService.Options;

namespace ZtmDataService.Commands.RefreshDataCommand.Rest
{
    public class RoutesRestClient : CommonRestClient<Route>
    {
        public RoutesRestClient(IOptions<ConnectionOptions> connectionOpts)
            : base(new RestClient<Route>(
                connectionOpts.Value.ApiBaseAddress,
                "dataset/c24aa637-3619-4dc2-a171-a23eec8f2172/resource/22313c56-5acf-41c7-a5fd-dc5dc72b3851/download/routes.json"))
        {
        }

        public async Task<IList<Route>> GetAll()
        {
            return await GetAll("routes");
        }
    }
}
