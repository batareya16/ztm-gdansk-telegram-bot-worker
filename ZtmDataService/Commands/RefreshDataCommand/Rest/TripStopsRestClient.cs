using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using ZtmDataService.Data.Models;
using ZtmDataService.Http;
using ZtmDataService.Options;

namespace ZtmDataService.Commands.RefreshDataCommand.Rest
{
    public class TripStopsRestClient : CommonRestClient<TripStop>
    {
        public TripStopsRestClient(IOptions<ConnectionOptions> connectionOpts)
            : base(new RestClient<TripStop>(
                connectionOpts.Value.ApiBaseAddress,
                "dataset/c24aa637-3619-4dc2-a171-a23eec8f2172/resource/3115d29d-b763-4af5-93f6-763b835967d6/download/stopsintrip.json"))
        {
        }

        public async Task<IList<TripStop>> GetAll()
        {
            return await GetAll("stopsInTrip");
        }
    }
}
