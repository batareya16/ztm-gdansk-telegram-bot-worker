using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using ZtmDataService.Data.Models;
using ZtmDataService.Http;

namespace ZtmDataService.Commands.RefreshDataCommand.Rest
{
    public class StopsTimeRestClient : CommonRestClient<StopTime>
    {
        private readonly DateTime dateTime;

        public StopsTimeRestClient(DateTime dateTime, long routeId)
            : base(new RestClient<StopTime>(
                "https://ckan2.multimediagdansk.pl/",
                $"stopTimes?date={dateTime:yyyy-MM-dd}&routeId={routeId}"))
        {
            this.dateTime = dateTime;
        }

        public async Task<StopTime[]> GetAll()
        {
            var stopTimes =
                JObject.Parse(await RestClient.GetAsStringAsync())
                    .Property("stopTimes")
                    .Value
                    .ToObject<StopTime[]>();

            foreach (var stopTime in stopTimes)
            {
                stopTime.ValidDate = dateTime;
            }

            return stopTimes;
        }
    }
}
