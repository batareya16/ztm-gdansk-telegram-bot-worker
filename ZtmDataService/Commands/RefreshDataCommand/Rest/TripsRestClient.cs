using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using ZtmDataService.Data.Models;
using ZtmDataService.Http;
using ZtmDataService.Options;

namespace ZtmDataService.Commands.RefreshDataCommand.Rest
{
    public class TripsRestClient : CommonRestClient<Trip>
    {
        public TripsRestClient(IOptions<ConnectionOptions> connectionOpts)
            : base(new RestClient<Trip>(
                connectionOpts.Value.ApiBaseAddress,
                "dataset/c24aa637-3619-4dc2-a171-a23eec8f2172/resource/b15bb11c-7e06-4685-964e-3db7775f912f/download/trips.json"))
        {
        }

        public async Task<IList<Trip>> GetAll()
        {
            return await GetAll("trips");
        }
    }
}
