using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using ZtmDataService.Commands.RefreshDataCommand.Rest;
using ZtmDataService.Data;
using ZtmDataService.Data.Models;
using ZtmDataService.Options;

namespace ZtmDataService.Commands.RefreshDataCommand
{
    public class RefreshDataCommand : ICommand
    {
        public async Task DoAction(IServiceScope scope)
        {
            var connectionOpts = scope.ServiceProvider.GetService<IOptions<ConnectionOptions>>();
            var httpStopsClient = new StopsRestClient(connectionOpts);
            var allStops = await httpStopsClient.GetAll();

            var httpRoutesClient = new RoutesRestClient(connectionOpts);
            var allRoutes = (await httpRoutesClient.GetAll())
                .Where(x => x.Type == "BUS" ||  x.Type == "TRAM")
                .ToList();

            var httpTripsClient = new TripsRestClient(connectionOpts);
            var allTrips = (await httpTripsClient.GetAll())
                .Where(x => x.RouteType == "MAIN" || x.RouteType == "SIDE");

            var httpTripStopsClient = new TripStopsRestClient(connectionOpts);
            var allTripStops = (await httpTripStopsClient.GetAll())
                .Where(x => x.IsPassenger == true);

            using (scope.ServiceProvider.GetService<TransactionContext>())
            {
                var dbFunctions = scope.ServiceProvider.GetService<IDatabaseFunctions>();
                dbFunctions.ClearDatabase();
                dbFunctions.ConnectFuzzySearchModule();
                scope.ServiceProvider.GetService<IRepository<Stop>>().InsertRange(allStops);
                scope.ServiceProvider.GetService<IRepository<Route>>().InsertRange(allRoutes);
                scope.ServiceProvider.GetService<IRepository<Trip>>().InsertRange(allTrips);
                scope.ServiceProvider.GetService<IRepository<TripStop>>().InsertRange(allTripStops);
                foreach (var stopsTimeClient in allRoutes.Select(route => new StopsTimeRestClient(route.ValidDate, route.Id)))
                {
                    scope.ServiceProvider.GetService<IRepository<StopTime>>().InsertRange((await stopsTimeClient
                        .GetAll()).Where(x => x.IsPassenger == true));
                }
            }
        }
    }
}
