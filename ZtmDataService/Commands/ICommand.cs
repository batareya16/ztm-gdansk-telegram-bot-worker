using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace ZtmDataService.Commands
{
    public interface ICommand
    {
        public Task DoAction(IServiceScope scope);
    }
}
