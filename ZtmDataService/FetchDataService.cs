using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ZtmDataService.Commands.RefreshDataCommand;
using ZtmDataService.Options;

namespace ZtmDataService
{
    public class FetchDataService : IHostedService, IDisposable
    {
        private int executionCount;
        private readonly ILogger<FetchDataService> logger;
        private readonly RefreshDataCommand refreshDataCommand;
        private readonly IServiceProvider serviceProvider;
        private Timer timer;

        public FetchDataService(
            ILogger<FetchDataService> logger,
            RefreshDataCommand refreshDataCommand,
            IServiceProvider serviceProvider)
        {
            this.logger = logger;
            this.refreshDataCommand = refreshDataCommand;
            this.serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            var workerOpts = serviceProvider.GetService<IOptions<WorkerOptions>>().Value;
            logger.LogInformation("Data fetching timed hosted service is running..");
            timer = new Timer(DoWork, null, TimeSpan.FromHours(workerOpts.DueHours), TimeSpan.FromHours(workerOpts.Period));
            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            var count = Interlocked.Increment(ref executionCount);
            using (var scope = serviceProvider.CreateScope())
            {
                refreshDataCommand.DoAction(scope).Wait();
            }
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            logger.LogInformation(
                "Timed Hosted Service is working. Last exec time: {ElapsedMs} ms, No: {Count}",
                elapsedMs,
                count);
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation("Timed Hosted Service is stopping.");

            timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            timer?.Dispose();
        }
    }
}
