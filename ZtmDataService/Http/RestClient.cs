using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ZtmDataService.Http
{
    public sealed class RestClient<TResource> : IDisposable where TResource : class
    {
        private readonly HttpClient httpClient;
        private readonly string _addressSuffix;
        private bool disposed;

        public RestClient(string baseAddress, string addressSuffix)
        {
            _addressSuffix = addressSuffix;
            httpClient = CreateHttpClient(baseAddress);
        }

        private static HttpClient CreateHttpClient(string serviceBaseAddress) =>
            new HttpClient {BaseAddress = new Uri(serviceBaseAddress)};

        public async Task<TResource> GetAsync()
        {
            using var responseMessage = await httpClient.GetAsync(_addressSuffix);
            responseMessage.EnsureSuccessStatusCode();
            return await responseMessage.Content.ReadAsAsync<TResource>();
        }

        public async Task<string> GetAsStringAsync()
        {
            using var responseMessage = await httpClient.GetAsync(_addressSuffix);
            responseMessage.EnsureSuccessStatusCode();
            return await responseMessage.Content.ReadAsStringAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed || !disposing)
            {
                return;
            }

            httpClient?.Dispose();
            disposed = true;
        }
    }
}
