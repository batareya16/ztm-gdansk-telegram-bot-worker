using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ZtmDataService.Commands.RefreshDataCommand;
using ZtmDataService.Data;
using ZtmDataService.Options;

namespace ZtmDataService
{
    public class Program
    {
        private static IConfigurationRoot Configuration { get; set; }

        public static void Main(string[] args)
        {
            CreateConfiguration();
            var host = CreateHostBuilder(args).Build();
            CreateDbIfNotExists(host);
            host.Run();
        }

        private static void CreateConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        private static void CreateDbIfNotExists(IHost host)
        {
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;
            try
            {
                var context = services.GetRequiredService<DatabaseContext>();
                context.Database.EnsureCreated();
            }
            catch (Exception ex)
            {
                var logger = services.GetRequiredService<ILogger<Program>>();
                logger.LogError(ex, "An error occurred creating the DB");
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<ConnectionOptions>(options => Configuration.GetSection(nameof(ConnectionOptions)).Bind(options));
                    services.Configure<WorkerOptions>(options => Configuration.GetSection(nameof(WorkerOptions)).Bind(options));
                    services.AddHostedService<FetchDataService>();
                    services.AddDbContext<DatabaseContext> (
                        options => options.UseNpgsql(Configuration
                            .GetSection(nameof(ConnectionOptions))
                            .GetSection(nameof(ConnectionOptions.ConnectionString)).Value),
                        ServiceLifetime.Transient,
                        ServiceLifetime.Transient);
                    services.AddTransient<TransactionContext>();
                    services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
                    services.AddTransient<IDatabaseFunctions, DatabaseFunctions>();
                    services.AddTransient<RefreshDataCommand>();
                });
    }
}
